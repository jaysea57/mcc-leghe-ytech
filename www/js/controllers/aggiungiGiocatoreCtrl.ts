/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />


(function () {
	var app = angular.module('aggiungiGiocatoreCtrl', []);
	app.controller('aggiungiGiocatoreCtrl', function ($scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, $ionicModal, $timeout,
		$location, $ionicScrollDelegate, $cordovaGoogleAnalytics,
		FantaCalcioServices: OM.FantaCalcioFactory, RosaService: OM.RosaService) {
		$scope.vm = {
			portieriRosa: [],
			totalePortieri: 0,
			difensoriRosa: [],
			totaleDifensori: 0,
			centrocampistiRosa: [],
			totaleCentrocampisti: 0,
			attaccantiRosa: [],
			totaleAttaccanti: 0,
			allenatoriRosa: [],
			totaleAllenatori: 0,
			rosaShown: '',
			//indiceShown: -1,
			saldo: 0,
			//venduti: '',
			acquistati: '',
			giocatoriDisponibili: [],
			squadreFiltro: [],
			//variabili usate solo per la visualizzazione
			//bloccoRosa: false,
			//totaleRosa: 0,
			//portieriLength: 0,
			//difensoriLength: 0,
			//centrocampistiLength: 0,
			//attaccantiLength: 0,
			//allenatoriLength: 0,
			//totaleCosto: 0
		};			
		$scope.filter = {
			ordinaPerNome: true,
			ordinaCrescente: true,
			filtroSquadra: [],
			minCosto: 1,
			maxCosto: 46,
		}

		function aggiungiGiocatoreInit(){
			console.log(">aggiungiGiocatoreInit()");
			$ionicModal.fromTemplateUrl('filtroModal.html', {
				scope: $scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				$scope.modal = modal;
				if($rootScope.misterCalcioCup)
					var filter = JSON.parse(window.localStorage.getItem('misterCalcioCupFilterSettings'+$rootScope.user.idCliente));
				else
					var filter = JSON.parse(window.localStorage.getItem('tuttoSportLeagueFilterSettings'+$rootScope.user.idCliente));
				if(filter)
					$scope.filter = filter;
				var ordinaPer = '';
				if($scope.filter.ordinaPerNome)
					ordinaPer = 'cognome';
				else
					ordinaPer = 'costo';
				switch($stateParams.ruoloGiocatore){
					case '0':
						if (window.cordova) $cordovaGoogleAnalytics.trackView('Lista Portieri');
						$scope.vm.giocatoriDisponibili = RosaService.getPortieri(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
						break;
					case '1':
						if (window.cordova) $cordovaGoogleAnalytics.trackView('Lista Difensori');
						$scope.vm.giocatoriDisponibili = RosaService.getDifensori(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
						break;
					case '2':
						if (window.cordova) $cordovaGoogleAnalytics.trackView('Lista Centrocampisti');
						$scope.vm.giocatoriDisponibili = RosaService.getCentrocampisti(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
						break;
					case '3':
						if (window.cordova) $cordovaGoogleAnalytics.trackView('Lista Attaccanti');
						$scope.vm.giocatoriDisponibili = RosaService.getAttaccanti(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
						break;
					case '4':
						if (window.cordova) $cordovaGoogleAnalytics.trackView('Lista Allenatori');
						$scope.vm.giocatoriDisponibili = RosaService.getAllenatori(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
						break;
				}
			});
			$scope.vm.saldo = RosaService.getSaldo();
			$scope.vm.acquistati = RosaService.getGiocatoriAcquistati();
			$scope.openModalFiltri = function() {
				
				if (window.cordova) $cordovaGoogleAnalytics.trackView('Filtro Giocatori Rosa');
				
				$scope.modal.show();
				if ($('#range-costo .irs').length == 0){
					var $range: any = $("#range");
					$range.ionRangeSlider({
						hide_min_max: false,
						min: 1,
						max: 46,
						from: $scope.filter.minCosto,
						to: $scope.filter.maxCosto,
						type: 'double',
						step: 1,
						min_interval: 1,
						onFinish: function(data:any){
							$scope.filter.minCosto = data.from;
							$scope.filter.maxCosto = data.to;
							$scope.$apply();
						}
					});
				}
			};
			$scope.annullaModalFiltri = function() {
				$scope.modal.hide();
			};
			$scope.confermaModalFiltri = function() {
				$scope.vm.giocatoriDisponibili = [];
				$timeout(function() {
					var filter = $scope.filter;
					if($rootScope.misterCalcioCup)
						window.localStorage.setItem('misterCalcioCupFilterSettings'+$rootScope.user.idCliente, JSON.stringify(filter));
					else
						window.localStorage.setItem('tuttoSportLeagueFilterSettings'+$rootScope.user.idCliente, JSON.stringify(filter));
					var ordinaPer = '';
					if($scope.filter.ordinaPerNome)
						ordinaPer = 'cognome';
					else
						ordinaPer = 'costo';
					switch($stateParams.ruoloGiocatore){
						case '0':
							$scope.vm.giocatoriDisponibili = RosaService.getPortieri(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
							break;
						case '1':
							$scope.vm.giocatoriDisponibili = RosaService.getDifensori(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
							break;
						case '2':
							$scope.vm.giocatoriDisponibili = RosaService.getCentrocampisti(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
							break;
						case '3':
							$scope.vm.giocatoriDisponibili = RosaService.getAttaccanti(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
							break;
						case '4':
							$scope.vm.giocatoriDisponibili = RosaService.getAllenatori(ordinaPer, $scope.filter.ordinaCrescente, $scope.filter.filtroSquadra, $scope.filter.minCosto, $scope.filter.maxCosto);
							break;
					}
					$scope.modal.hide();
					$ionicScrollDelegate.$getByHandle('contentScroll').resize();
			    }, 10, false);
			};
			$ionicModal.fromTemplateUrl('filtroSquadre.html', {
				scope: $scope,
				animation: 'slide-in-right'
			}).then(function(modal) {
				$scope.modalSquadre = modal;
				$scope.vm.squadreFiltro = RosaService.getSquadre();
			});
			$scope.openModalSquadre = function() {
				$scope.modalSquadre.show();
			};
			$scope.pulisciSquadre = function() {
				$scope.filter.filtroSquadra.splice(0, $scope.filter.filtroSquadra.length);
			};
			$scope.closeModalSquadre = function() {
				var filter = $scope.filter;
				if($rootScope.misterCalcioCup)
					window.localStorage.setItem('misterCalcioCupFilterSettings'+$rootScope.user.idCliente, JSON.stringify(filter));
				else
					window.localStorage.setItem('tuttoSportLeagueFilterSettings'+$rootScope.user.idCliente, JSON.stringify(filter));
				$scope.modalSquadre.hide();
			};
			//Cleanup the modalS when we're done with it!
			$scope.$on('$destroy', function() {
				if($scope.modal){
					$scope.modal.remove();
					$scope.modalSquadre.remove();
				}
			});			
		};
		$scope.$on('$ionicView.afterEnter', function(){
			$rootScope.hideLoading();
		});
		$scope.selectGiocatore = function(giocatore: OM.GIOCATORIDISPONIBILI){
			if(RosaService.getOfferte() == false){
				var confirmText: string = ("Sei sicuro di voler acquistare " + giocatore.COGNOME + " ? La sua quotazione è di " + giocatore.VALORE_ATTUALE + " crediti");
				if($scope.vm.saldo >= giocatore.VALORE_ATTUALE){
					if(window.cordova){
						navigator.notification.confirm(confirmText, function(buttonPressed: number){
								if(buttonPressed == 1){
									RosaService.setGiocatore(giocatore, giocatore.VALORE_ATTUALE);
									$scope.vm.acquistati += giocatore.ID + ';';
									RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
									$scope.vm.saldo -= giocatore.VALORE_ATTUALE;
									RosaService.setSaldo($scope.vm.saldo);
									$ionicHistory.goBack();
								}
							}, 'Acquista giocatore', ['Conferma', 'Annulla']);
					} else if(confirm(confirmText)) {
						RosaService.setGiocatore(giocatore, giocatore.VALORE_ATTUALE);
						$scope.vm.acquistati += giocatore.ID + ';';
						RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						$scope.vm.saldo -= giocatore.VALORE_ATTUALE;
						RosaService.setSaldo($scope.vm.saldo);
						$ionicHistory.goBack();
					}
				} else {
					if(window.cordova){
						navigator.notification.alert('Offerta non consentita. Crediti insufficienti', function(){}, 'Acquista giocatore');
					} else {
						alert('Offerta non consentita. Crediti insufficienti');
					}
				}
			} else {
				if(!ionic.Platform.isIOS()){
					var placeHolderText: string = "Inserisci l'offerta (valore attuale "+ giocatore.VALORE_ATTUALE.toString()+")";
				} else {
					var placeHolderText: string = '' + giocatore.VALORE_ATTUALE;
				}
				var promptText: string = 'Quanto offri per l\'acquisto di ' + giocatore.COGNOME + ' ' + giocatore.NOME + 
					' ? (saldo attuale: ' + $scope.vm.saldo + ' crediti)';
				if(window.cordova){
					navigator.notification.prompt(promptText, function(results: PromptResult){
						if(results.buttonIndex != 2){
							var offerta: any = results.input1;
							if(offerta != null){
								offerta = parseInt(offerta);
								if(offerta > $scope.vm.saldo){
									navigator.notification.alert('Offerta non consentita. Crediti insufficienti', function(){}, 'Acquista giocatore');
								} else if(isInteger(offerta) && offerta > 0){
									RosaService.setGiocatore(giocatore, offerta);
									$scope.vm.acquistati += giocatore.ID + ':' + offerta + ';';
									RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
									$scope.vm.saldo -= offerta;
									RosaService.setSaldo($scope.vm.saldo);
									$ionicHistory.goBack();
								} else if (offerta != null && (offerta <= 0 || !isInteger(offerta))){
									navigator.notification.alert('Inserisci un\'offerta valida!', function(){}, 'Acquista giocatore');
								}
							}
						}
					}, 'Acquista giocatore', ['Conferma', 'Annulla'], placeHolderText);
				} else {
					var offerta: any = prompt(promptText, giocatore.VALORE_ATTUALE.toString());
					if(offerta != null){
						offerta = parseInt(offerta);
						if(offerta > $scope.vm.saldo){
							alert('Offerta non consentita. Crediti insufficienti');
						} else if(isInteger(offerta) && offerta > 0){
							RosaService.setGiocatore(giocatore, offerta);
							$scope.vm.acquistati += giocatore.ID + ':' + offerta + ';';
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
							$scope.vm.saldo -= offerta;
							RosaService.setSaldo($scope.vm.saldo);
							$ionicHistory.goBack();
						} else if (offerta != null && (offerta <= 0 || !isInteger(offerta))){
							alert('Inserisci un\'offerta valida!');
						}
					}
				}
			}
		};

		$scope.firstLetter = function(text: string){
			if(text)
				return text.charAt(0)+'.';
			else
				return '';
		};

		$scope.$on('$ionicView.beforeEnter', aggiungiGiocatoreInit());

	});

	function isInteger(x) {
		return x % 1 === 0;
	}

})();
