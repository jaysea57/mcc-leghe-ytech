/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('requiredRegCtrl', []);
    app.controller('requiredRegCtrl', function ($scope, $rootScope, $state, $stateParams, $ionicLoading, $ionicModal, $ionicScrollDelegate,
        $ionicHistory, $cordovaFileTransfer, FantaCalcioServices: OM.FantaCalcioFactory) {

        var torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
            //TUTTO SPORT LEAGUE CLASSICO
                //?'http://localhost:9292/league.tsport.yland.it/'
                //:'http://league.tsport.yland.it/';                        //ESTERNA
                //:'http://devts.tuttosport.com/';                              //RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP CLASSICO
                ?'http://localhost:9292/gp.mr.yland.it/'
                //:'http://gp.mr.yland.it/'                                 //RETE ESTERNA
                :'http://gp-mistercalciocup.corrieredellosport.it/';        //PRODUZIONE
                //:'http://devgp.corrieredellosport.it/'                        //RETE AZIENDALE Y TECH
                ;

        var torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
            //TUTTO SPORT LEAGUE GIRONI modo_gioco = 1
                //?'http://localhost:9292/gironi.tsport.yland.it/'
                //:'http://gironi.tsport.yland.it/';                        //ESTERNA
                //:'http://devtsgironi.tuttosport.com/';                    //RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP GIRONI modo_gioco = 1
                ?'http://localhost:9292/www.mr.yland.it/'
                //:'http://www.mr.yland.it/';                               //ESTERNA
                :'http://mistercalciocup.corrieredellosport.it/'        //PRODUZIONE
                //:'http://devmcc.corrieredellosport.it/'                           //RETE AZIENDALE Y TECH
                ;

        $scope.vm = {
            email: '',
            accettaRegolamentoDiGioco: false,
            accettaCondizioniEditore: false,
            accettaCondizioniGioco: false,
            accettaPrivacy1: false,
            accettaPrivacy2: false,
            accettaPrivacy3: false,
        };

        $scope.facebookLogin = function(_data){
            FantaCalcioServices.facebookAuthentication(
                _data.user.email,
                '', //password
                $rootScope.modoGioco,
                _data.user.accessToken
                , $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget
            ).then(function(data:OM.AuthenticationResponse){
                if (data.success){
                    console.log(data);
                    //$rootScope.modoGioco = $scope.vm.modoGioco;
                    if ($rootScope.misterCalcioCup){
                        window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify($scope.vm));
                    } else {
                        window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify($scope.vm));
                    }
                    $rootScope.user = data.success.user;
                    $rootScope.accessToken = data.success.user.accessToken;
                    if (data.success.required_reg == 1){
                        if (data.success.required_email == 1)
                            $state.go('requiredReg', {requiredMail: 1});
                        else
                            $state.go('requiredReg', {requiredMail: 0});
                    } else {
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true,
                            historyRoot: true
                        });
                        $state.go('app.home');
                    }
                } else if(data.error){
                    console.log(data);
                    $rootScope.hideLoading();
                    $rootScope.showToast(data.error.user_message, 5000);
                }
                $rootScope.hideLoading();
            }, function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };

        $scope.registrazione = function(){
            $rootScope.showLoading();
            FantaCalcioServices.registraUtenteFB($scope.vm.email, $rootScope.accessToken, $scope.vm.accettaRegolamentoDiGioco, $scope.vm.accettaPrivacy2, $scope.vm.accettaPrivacy3, $scope.vm.accettaCondizioniGioco, $scope.vm.accettaCondizioniEditore, 
                $scope.vm.accettaPrivacy1, $rootScope.modoGioco).then(function(data){
                if(data.success){
                    $scope.facebookLogin(data.success);
                }else if(data.error){
                    console.log(data);
                    $rootScope.hideLoading();
                    $rootScope.showToast(data.error.user_message, 5000);
                }
            },function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            });
        };

        $scope.goBack = function(){
            window.history.back();
        };

        $scope.registrazioneInit = function(){
            $scope.requiredMail = $stateParams.requiredMail;
            $scope.vm.email = $rootScope.user.email;
            
            if($rootScope.modoGioco == 3) {
                $scope.urlDomain = torneoClassicoUrl;
            } else {
                $scope.urlDomain = torneoGironiUrl;
            }
            $scope.openRegolamento = function() {
                if(ionic.Platform.isIOS()){
                    $rootScope.hideLoading();
                    window.open($scope.urlDomain+'doc/regolamento_gioco_leghe.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");                
                    //window.open($scope.urlDomain+'doc/regolamento_concorso'+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");                
                } else if(ionic.Platform.isAndroid()){
                    var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/regolamento_gioco_leghe.pdf'));

                    var trustHosts = true
                    var options = {};

                    $cordovaFileTransfer.download(encodeURI($scope.urlDomain+'doc/regolamento_gioco_leghe.pdf'),
                        filePath, options, trustHosts).then(function(fileEntry){
                            console.log("Download success");
                            console.log(fileEntry);
                            $rootScope.hideLoading();
                            window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
                        }, function(error){
                            $rootScope.hideLoading();
                            navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
                            console.log("Download error");
                            console.log(error);
                        });
                }
            };
            $scope.openServizioEditore = function() {
                if(ionic.Platform.isIOS()){
                    $rootScope.hideLoading();
                    window.open($scope.urlDomain+'doc/condizioni_generali_utilizzo.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");               
                } else if(ionic.Platform.isAndroid()){                  
                    var fileTransfer = new window['FileTransfer']();
                    var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + "Download/condizioni_generali_utilizzo.pdf");
                    fileTransfer.download(encodeURI($scope.urlDomain+'doc/condizioni_generali_utilizzo.pdf'),
                        filePath,
                        function(fileEntry){
                            console.log("Download success");
                            console.log(fileEntry);
                            window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
                        },
                        function(error){
                            console.log("Download error");
                            console.log(error);
                        }
                    );
                }
            };
            $scope.openServizioGioco = function() {
                if(ionic.Platform.isIOS()){
                    $rootScope.hideLoading();
                    window.open($scope.urlDomain+('doc/condizioni_generali_utilizzo')+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");              
                } else if(ionic.Platform.isAndroid()){
                    var fileTransfer = new window['FileTransfer']();
                    var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/condizioni_generali_utilizzo')+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf');
                    fileTransfer.download(encodeURI($scope.urlDomain+('doc/condizioni_generali_utilizzo')+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf'),
                        filePath,
                        function(fileEntry){
                            console.log("Download success");
                            console.log(fileEntry);
                            window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
                        },
                        function(error){
                            console.log("Download error");
                            console.log(error);
                        }
                    );
                }
            };
            $scope.openInformativaPrivacy = function() {
                if(ionic.Platform.isIOS()){
                    $rootScope.hideLoading();
                    window.open($scope.urlDomain+'doc/informativa_cds.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");                
                } else if(ionic.Platform.isAndroid()){
                    var fileTransfer = new window['FileTransfer']();
                    var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + "Download/condizioni_generali_utilizzo.pdf");
                    fileTransfer.download(encodeURI($scope.urlDomain+'doc/informativa_cds.pdf'),
                        filePath,
                        function(fileEntry){
                            console.log("Download success");
                            console.log(fileEntry);
                            window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
                        },
                        function(error){
                            console.log("Download error");
                            console.log(error);
                        }
                    );
                }
            };
            $rootScope.hideLoading();
        };


    });

})();
