/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('calendarioCtrl', []);
    app.controller('calendarioCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $cordovaGoogleAnalytics,
    	$ionicPopover, FantaCalcioServices: OM.FantaCalcioFactory) {
    	$scope.vm = {
            numeroGiornata: $stateParams.numeroGiornata,
            dataGiornata: $stateParams.dataGiornata,
            giornate: [],
            portieriRosa: [],
            portieriRosaPanchina: [],
            difensoriRosa: [],
            difensoriRosaPanchina: [],
            centrocampistiRosa: [],
            centrocampistiRosaPanchina: [],
            attaccantiRosa: [],
            attaccantiRosaPanchina: [],
            allenatoriRosa: [],
            allenatoriRosaPanchina: [],
            loadingOk: null,
            errorMessage: ''
        };

        $scope.initCalendario = function(){
            console.log(">initCalendario");
            if (window.cordova) {
                $cordovaGoogleAnalytics.trackView('Calendario');
            }

            FantaCalcioServices.getCalendario($rootScope.squadraSelected.codice_squadra).then(function(data){
                if(data.msg_utente){
                    $rootScope.hideLoading();
                    $scope.vm.loadingOk = false;
                    $scope.vm.errorMessage = data.msg_utente;
                    //$rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
                }else{
                    //$rootScope.showToast(data.msg_utente);
                    console.log(data);
                    if(data.giornate){
                        $scope.vm.loadingOk = true;
                        $scope.vm.giornate = data.giornate;
                    }
                    $rootScope.hideLoading();
                }
            }, function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
                console.log(data);
            });
        };
        $scope.goToDettaglioGiornata = function(giornata){
            $rootScope.showLoading();
            if($rootScope.modoGioco == 3){
                $rootScope.giornataClassico = giornata;
                $state.go('app.dettaglioGiornata', {
                    idGiornata: giornata.id_partita, 
                    numeroGiornata: giornata.numero, 
                    dataGiornata: giornata.data 
                });
            } else {
                $rootScope.giornataGironi = giornata;
                $state.go('app.partiteGironi');
            }
        };

    	$scope.initDettaglioGiornata = function(){
    		FantaCalcioServices.getDettaglioGiornata($rootScope.squadraSelected.codice_squadra, $rootScope.giornataClassico.id_partita)
    		.then(function(data){
    			if(data.error){
                    $rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
		        }else{
		        	console.log(data);
                    data.squadre[0].titolari.forEach(function(item){
                        switch(item.cd_ruolo){
                            case 0:
                                $scope.vm.portieriRosa.push(item);
                                break;
                            case 1:
                                $scope.vm.difensoriRosa.push(item);
                                break;
                            case 2:
                                $scope.vm.centrocampistiRosa.push(item);
                                break;
                            case 3:
                                $scope.vm.attaccantiRosa.push(item);
                                break;
                            case 4:
                                $scope.vm.allenatoriRosa.push(item);
                                break;
                        };
                    });
                    data.squadre[0].riserve.forEach(function(item){
                        switch(item.cd_ruolo){
                            case 0:
                                $scope.vm.portieriRosaPanchina.push(item);
                                break;
                            case 1:
                                $scope.vm.difensoriRosaPanchina.push(item);
                                break;
                            case 2:
                                $scope.vm.centrocampistiRosaPanchina.push(item);
                                break;
                            case 3:
                                $scope.vm.attaccantiRosaPanchina.push(item);
                                break;
                            case 4:
                                $scope.vm.allenatoriRosaPanchina.push(item);
                                break;
                        };
                    });
		        }
                $rootScope.hideLoading();
			},function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
			});
    	};

        $scope.goToDettaglioTabellino = function(giocatore){
            $rootScope.giocatore = giocatore;
            if (giocatore.cd_ruolo == 0)
                giocatore.icona = 'icon--portiere';
            else if (giocatore.cd_ruolo == 1)
                giocatore.icona = 'icon--difensore';
            else if (giocatore.cd_ruolo == 2)
                giocatore.icona = 'icon--centrocampista';
            else if (giocatore.cd_ruolo == 3)
                giocatore.icona = 'icon--attaccante';
            else if (giocatore.cd_ruolo == 4)
                giocatore.icona = 'icon--allenatore';

            if (window.cordova) $cordovaGoogleAnalytics.trackView('Tabellino');

            $state.go('app.dettaglioTabellino');
        };

        $scope.firstLetter = function(text: string){
            if(text)
                return text.charAt(0)+'.';
            else
                return '';
        };

        // $ionicPopover.fromTemplateUrl('popoverPanchina.html', {
        //     scope: $scope,
        // }).then(function(popover) {
        //     $scope.popoverPanchina = popover;
        // });

    });
})();
