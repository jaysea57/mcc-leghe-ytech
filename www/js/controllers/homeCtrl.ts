/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function(){
	var app = angular.module('homeCtrl', []);

	app.controller('homeCtrl', function($scope, $rootScope, $state, $ionicLoading, $ionicHistory, $ionicModal, $cordovaGoogleAnalytics,
	FantaCalcioServices: OM.FantaCalcioFactory) {

	$rootScope.rosaInCorso = false;
	$scope.init = function(){
		console.log("init homeCtrl");
		if (window.cordova) {
			$cordovaGoogleAnalytics.trackView('Dashboard');
		}

		$ionicHistory.clearHistory;
		FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(data){
			if(data.error){
				$rootScope.showToast(data.error.user_message);
                if(data.error && data.error.id == 'not_logged')
                    $rootScope.autologin();
			}else{
				console.log(data);
				$rootScope.testoEdizioneDigitale = $rootScope.user.msgCodicePromo;
					$rootScope.squadreTorneoGironi = data.squadre;
					if($rootScope.misterCalcioCup){
						window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify(data.squadre));
					} else {
						window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify(data.squadre));
					}
				}
				
			if(!$rootScope.squadraSelected){
				$rootScope.squadraSelected = data.squadre[0];
			}
			else { // jc 2017
				for (let sq of data.squadre) {
					if (sq.codice_squadra == $rootScope.squadraSelected.codice_squadra) {
						console.log("faccio refresh of squadra selected");
						$rootScope.squadraSelected = sq;
					}
				}
			}
			
			if($rootScope.misterCalcioCup){
				window.localStorage.setItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
			} else {
				window.localStorage.setItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
			}
			if($rootScope.hideNews.value == false){
				$ionicModal.fromTemplateUrl('newsModal.html', {
					scope: $scope,
					animation: 'slide-in-up'
				}).then(function(modal) {
					$scope.modal = modal;
					$scope.news = [];
					FantaCalcioServices.getNews().then(function(data: OM.GetNewsResponse){
						if(data.listaNews && data.listaNews.length > 0){
							var newsRead: OM.News[] = [];
							if($rootScope.misterCalcioCup){
								newsRead = JSON.parse(window.localStorage.getItem('misterCalcioCupNewsRead'+$rootScope.user.idCliente));
							} else {
								newsRead = JSON.parse(window.localStorage.getItem('tuttoSportLeagueNewsRead'+$rootScope.user.idCliente));
							}
							$rootScope.hideLoading();
							if(newsRead){
								newsRead.forEach(function(itemRead){
									data.listaNews.forEach(function(itemLista, iLista){
										if(itemLista.idNews == itemRead.idNews){
											data.listaNews.splice(iLista,1);
										}
									});
								});
							}
							$scope.news = data.listaNews;
							if($scope.news.length > 0){
								$scope.modal.show();
							}
						}
					});
					$scope.closeNewsModal = function(){
						if($rootScope.hideNews.value == true){
							if($rootScope.misterCalcioCup){
								window.localStorage.setItem('misterCalcioCupNewsRead'+$rootScope.user.idCliente, JSON.stringify($scope.news));
							} else {
								window.localStorage.setItem('tuttoSportLeagueNewsRead'+$rootScope.user.idCliente, JSON.stringify($scope.news));
							} 
						}
						$scope.modal.hide(); 
					};
				});
			}
    		$rootScope.hideLoading();    		
	  	},function(data){
        	$rootScope.hideLoading();
			$rootScope.showToast('Errore nella connessione');
		})
	};
	$scope.goToFormazione = function(){
		$rootScope.showLoading();
		$state.go('app.formazione');
	};
	$scope.goToDettaglioGiornata = function(){
		console.log("goToDettaglioGiornata");
		$rootScope.showLoading();
		if($rootScope.modoGioco == 3){
			$rootScope.giornataClassico = {id_partita: $rootScope.squadraSelected.ultima_partita.id};
			console.log("go app.dettaglioGiornata");
	  		$state.go('app.dettaglioGiornata', {
	  			idGiornata: $rootScope.squadraSelected.ultima_partita.id, 
	  			dataGiornata: $rootScope.squadraSelected.ultima_partita.data,
	  			numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero
	  			});
		} else {
			console.log("go app.dettaglioGiornataGironi");
			$rootScope.partitaDettaglioGiornata = $rootScope.squadraSelected.ultima_partita;
	  		$state.go('app.dettaglioGiornataGironi', {idGiornata: $rootScope.squadraSelected.ultima_partita.id, 
	  			dataGiornata: $rootScope.squadraSelected.ultima_partita.data, numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero});
	  	}
	};
	$scope.goToGestioneRosa = function(){
		$rootScope.showLoading();
	  	$state.go('app.gestioneRosa');
	};
	$scope.goToClassificaGironi = function(){
		$rootScope.showLoading();
	  	$state.go('app.classificheGironi');
	};

  });

  })();