/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />

(function () {
	var app = angular.module('loginCtrl', []);
	app.controller('loginCtrl', function ($ionicPlatform, $scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, $cordovaGoogleAnalytics,
		FantaCalcioServices: OM.FantaCalcioFactory) {
		$scope.vm = {
			email: '',
			password: '',
			modoGioco: 2
		};
		$rootScope.modoGioco = 2;

		$scope.counterLogin = 0;

		$scope.loginInit = function(){
			
			$ionicPlatform.ready(function() {
				if (window.cordova) {
					if($cordovaGoogleAnalytics)
						$cordovaGoogleAnalytics.trackView('Login Lega Amici');
				}
			});

        	$rootScope.misterCalcioCup = true;
			if($rootScope.misterCalcioCup){
				var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupLegheLogin'));
			} else {
				var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueLegheLogin'));
			}
			if(loginData && loginData.email){
				$scope.vm.email = loginData.email;
				$scope.vm.password = loginData.password;
			}
		};

		$scope.login = function(){
			console.log("loginCtrl.login()");
			if($scope.counterLogin == 0)
				$rootScope.showLoading("Login in corso");
			$scope.counterLogin ++;
			FantaCalcioServices.authentication($scope.vm.email, $scope.vm.password, $scope.vm.modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(data:OM.AuthenticationResponse){
				if(data.success){
					console.log(data);
					$rootScope.modoGioco = $scope.vm.modoGioco;
					if($rootScope.misterCalcioCup){
						window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify($scope.vm));
					} else {
						window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify($scope.vm));
					}
					$rootScope.user = data.success.user;
					$ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
					$state.go('app.home');
				}else if(data.error){
					console.log(data);
					if ($scope.counterLogin <= 3) {
						$scope.login();
					} else {
						$rootScope.hideLoading();
						$rootScope.showToast(data.error.user_message, 5000);
					}
				}
			},function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore nella connessione');
			});
		};

		$scope.facebookLogin = function(){
			if(!ionic.Platform.isIOS()){
				$rootScope.showLoading();
			}
				facebookConnectPlugin.login(['email'], function(FBSuccessResponse){
					console.log("facebook login success data: ");
					console.log(FBSuccessResponse);
					FantaCalcioServices.facebookAuthentication($scope.vm.email, $scope.vm.password, 
						$scope.vm.modoGioco, FBSuccessResponse.authResponse.accessToken
						, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget
						).then(function(data:OM.AuthenticationResponse){
						if(data.success){
							console.log(data);
							$rootScope.modoGioco = $scope.vm.modoGioco;
							if($rootScope.misterCalcioCup){
								window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify($scope.vm));
							} else {
								window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify($scope.vm));
							}
							$rootScope.user = data.success.user;
							$rootScope.accessToken = data.success.user.accessToken;
							if(data.success.required_reg == 1){
								if(data.success.required_email == 1)
									$state.go('requiredReg', {requiredMail: 1});
								else
									$state.go('requiredReg', {requiredMail: 0});
							} else {
								$ionicHistory.nextViewOptions({
			                        disableAnimate: true,
			                        disableBack: true,
			                        historyRoot: true
			                    });
								$state.go('app.home');
							}
						}else if(data.error){
							console.log(data);
							$rootScope.hideLoading();
							$rootScope.showToast(data.error.user_message, 5000);
						}
						$rootScope.hideLoading();
					},function(data){
						$rootScope.hideLoading();
						$rootScope.showToast('Errore nella connessione');
					});
				}, function(FBErrorResponse){
					$rootScope.hideLoading();
					$rootScope.showToast("Errore nella connessione con facebook");
					console.log("facebook login error data: ");
					console.log(FBErrorResponse);
				});
		};

		$scope.goRegistrazione = function(){
			$rootScope.showLoading();
			$state.go('registrazione', {gioco: $scope.vm.modoGioco});
		};

		if($rootScope.hideLoading){
			$rootScope.hideLoading();
		}
	});
})();