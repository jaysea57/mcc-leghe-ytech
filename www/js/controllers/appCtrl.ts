/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function(){
	var app = angular.module('appCtrl', []);

	app.controller('AppCtrl', function($scope, $rootScope, $state, $ionicHistory, $cordovaFileTransfer, $timeout,
		FantaCalcioServices: OM.FantaCalcioFactory, $ionicLoading) {
		
		$rootScope.hideNews = {value: false};
		$rootScope.testoEdizioneDigitale = '';

		$scope.accordionMenu = false;
		$scope.openAccordion = function(){
			if($scope.accordionMenu == true)
				$scope.accordionMenu = false;
			else
				$scope.accordionMenu = true;
		};

		$scope.selectSquadra = function(squadra, modo){
			$rootScope.squadraSelected = squadra;
			$rootScope.modoGioco = modo;
			//$rootScope.user = $rootScope.userGironi;
			$scope.accordionMenu = false;
			$ionicHistory.nextViewOptions({
				disableAnimate: true,
				disableBack: true,
				historyRoot: true
			});
			$rootScope.showToast($rootScope.squadraSelected.nome);
			$state.go('app.home');
		};

		$scope.logout = function(){
			$rootScope.showLoading();
			if($rootScope.misterCalcioCup){
				window.localStorage.removeItem('misterCalcioCupLegheLogin');
			} else {
				window.localStorage.removeItem('tuttoSportLeagueLegheLogin');
			}
			FantaCalcioServices.logout($rootScope.user.idCliente).then(function(data){
				if(data.error){
					$rootScope.showToast(data.error.user_message);
				$rootScope.user = null;
				$rootScope.squadraSelected = null;
				$rootScope.squadreTorneoClassico = [];
				$rootScope.squadreTorneoGironi = [];
				$scope.accordionMenu = false;
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: true
					});
				$state.go('tipoGioco');
				} else {
					$rootScope.user = null;
					$rootScope.squadraSelected = null;
					$rootScope.squadreTorneoClassico = [];
					$rootScope.squadreTorneoGironi = [];
					$scope.accordionMenu = false;
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true,
						historyRoot: true
					});
					$state.go('login', {gioco: 2});
				}
			}, function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore di connessione');
			});
		};

		$scope.goToModificaProfilo = function(){
			$rootScope.showLoading();
			//$rootScope.user = user;
			$scope.accordionMenu = false;
			$state.go('app.modificaProfilo');
		};

		$rootScope.autologin = function(counter: number){
			if(counter != undefined){
				if(counter == 0)
					$rootScope.showLoading();
				counter++;
				if($rootScope.misterCalcioCup)
					var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupLegheLogin'));
				else
					var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueLegheLogin'));
				FantaCalcioServices.authentication(loginData.email, loginData.password, loginData.modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(data:OM.AuthenticationResponse){
					if(data.success){
						console.log(data);
						if($rootScope.misterCalcioCup){
							window.localStorage.setItem('misterCalcioCupLegheLogin', JSON.stringify(loginData));
						} else {
							window.localStorage.setItem('tuttoSportLeagueLegheLogin', JSON.stringify(loginData));
						}
						$rootScope.user = data.success.user;
						$ionicHistory.nextViewOptions({
							disableAnimate: true,
							disableBack: true,
							historyRoot: true
						});
						$state.go('app.home');
					} else if (data.error){
						console.log(data);
						if (counter <= 3) {
							$rootScope.autologin(counter);
						} else {
							if($rootScope.misterCalcioCup){
								window.localStorage.removeItem('misterCalcioCupLegheLogin');
							} else {
								window.localStorage.removeItem('tuttoSportLeagueLegheLogin');
							}
							$rootScope.hideLoading();
							$rootScope.showToast(data.error.user_message);
						}
					}
					$rootScope.hideLoading();
				},function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore di connessione');
				});
			} else {
				$rootScope.autologin(0);
			}
		};

		$scope.goToState = function(state: string){
			if($state.current.name != state){
				$rootScope.showLoading();
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: false
				});
				$state.go(state);
			};
		};

		$scope.openRegolamento = function() { // regolamento_concorso_xxx.pdf
			$rootScope.showLoading();
			var urlDomain = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
			//TUTTO SPORT LEAGUE LEGHE modo_gioco = 2
				//?'http://localhost:9292/www.mr.yland.it/'
				//:'http://www.mr.yland.it/';       							//ESTERNA
			//MISTER CALCIO CUP LEGHE modo_gioco = 2  
				?'http://localhost:9292/www.mr.yland.it/'
				//:'http://www.mr.yland.it/';								//ESTERNA
				:'http://mistercalciocup.corrieredellosport.it/';		//PRODUZIONE
				;

			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
                window.open(urlDomain+'doc/regolamento_gioco_leghe.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");                
				//window.open(urlDomain+'doc/regolamento_concorso'+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/regolamento_gioco_leghe.pdf'));

				var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI(urlDomain+'doc/regolamento_gioco_leghe.pdf'),
					filePath, options, trustHosts).then(function(fileEntry){
						console.log("Download success");
						console.log(fileEntry);
						$rootScope.hideLoading();
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					}, function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
			else if (!window.cordova) {
				console.log("reg in browser");
				$rootScope.hideLoading();
                window.open(urlDomain+'doc/regolamento_gioco_leghe.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");                
			}
		};

		$scope.openEdizioneDigitale = function(){
			if(window.cordova){
				navigator.notification.alert($rootScope.testoEdizioneDigitale, undefined, 
					'Leghe Amici', 'OK');
			} else {
				alert($rootScope.testoEdizioneDigitale);
			}
		};

		$scope.creaLegaAmici = function(){
			$rootScope.showLoading();
			FantaCalcioServices.creaLega($rootScope.user.idCliente).then(function(legaData){
				if(legaData.success){
					$rootScope.user.idCampionato = legaData.success.id_campionato;
					$rootScope.user.idGruppo = legaData.success.id_gruppo;
					$rootScope.user.presidente = true;
					$rootScope.showToast(legaData.success.msg_utente);
					var squadraNuova = legaData.success.id_squadra;
					FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(dataSquadre){
						if(dataSquadre.error){
			    			$rootScope.hideLoading();    		
							$rootScope.showToast(dataSquadre.error.user_message);
						}else{
							console.log(dataSquadre);
							$rootScope.squadreTorneoGironi = dataSquadre.squadre;
							$rootScope.squadreTorneoGironi.some(function(item){
								if(item.codice_squadra == squadraNuova){
									$rootScope.squadraSelected = item;
									return true;
								}
							});
							$ionicHistory.nextViewOptions({
								disableAnimate: true,
								disableBack: true,
								historyRoot: true
							});
							$scope.accordionMenu = false;
							$state.go('app.datiLega');
						}
				  	},function(dataSquadre){
						$state.go('app.home');
					})
				} else {
					$rootScope.showToast(legaData.error.user_message);
					$rootScope.hideLoading();
				}
			},function(legaData){
				$rootScope.hideLoading();
			});
		};

	});

  })();