/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('dettaglioGiornataGironiCtrl', []);
    app.controller('dettaglioGiornataGironiCtrl', function ($scope, $rootScope, $stateParams, $state, $ionicLoading, $cordovaGoogleAnalytics,
    	$ionicPopover, FantaCalcioServices: OM.FantaCalcioFactory) {
    	$scope.vm = {
            numeroGiornata: $stateParams.numeroGiornata,
            idGiornata: $stateParams.idGiornata,
            dataGiornata: $stateParams.dataGiornata,
            giornate: [],
            squadra1: {
                hasFormazione: null,
                nome: '',
                portieriRosa: [],
                portieriRosaPanchina: [],
                difensoriRosa: [],
                difensoriRosaPanchina: [],
                centrocampistiRosa: [],
                centrocampistiRosaPanchina: [],
                attaccantiRosa: [],
                attaccantiRosaPanchina: [],
                allenatoriRosa: [],
                allenatoriRosaPanchina: []
            },
            squadra2: {
                hasFormazione: null,
                nome: '',
                portieriRosa: [],
                portieriRosaPanchina: [],
                difensoriRosa: [],
                difensoriRosaPanchina: [],
                centrocampistiRosa: [],
                centrocampistiRosaPanchina: [],
                attaccantiRosa: [],
                attaccantiRosaPanchina: [],
                allenatoriRosa: [],
                allenatoriRosaPanchina: []
            },
            squadraSelected: {}
        };

    	$scope.initDettaglioGiornataGironi = function(){
            console.log("initDettaglioGiornataGironi");
            if(!$rootScope.partitaDettaglioGiornata.id_partita){
                $rootScope.partitaDettaglioGiornata.id_partita = $rootScope.partitaDettaglioGiornata.id;
            }
    		FantaCalcioServices.getDettaglioGiornata($rootScope.partitaDettaglioGiornata.squadre[0].id, $rootScope.partitaDettaglioGiornata.id_partita)
    		.then(function(data){
    			if(data.error){
                    $rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
		        }else{
		        	console.log(data);
                    data.squadre.forEach(function(squadra, index){
                        $scope.vm['squadra'+(index+1)].nome = squadra.nome;
                        if(squadra.titolari && squadra.riserve){
                            $scope.vm['squadra'+(index+1)].hasFormazione = true;
                            squadra.titolari.forEach(function(item){
                                switch(item.cd_ruolo){
                                    case 0:
                                        $scope.vm['squadra'+(index+1)].portieriRosa.push(item);
                                        break;
                                    case 1:
                                        $scope.vm['squadra'+(index+1)].difensoriRosa.push(item);
                                        break;
                                    case 2:
                                        $scope.vm['squadra'+(index+1)].centrocampistiRosa.push(item);
                                        break;
                                    case 3:
                                        $scope.vm['squadra'+(index+1)].attaccantiRosa.push(item);
                                        break;
                                    case 4:
                                        $scope.vm['squadra'+(index+1)].allenatoriRosa.push(item);
                                        break;
                                    };
                            });
                            squadra.riserve.forEach(function(item){
                                switch(item.cd_ruolo){
                                    case 0:
                                        $scope.vm['squadra'+(index+1)].portieriRosaPanchina.push(item);
                                        break;
                                    case 1:
                                        $scope.vm['squadra'+(index+1)].difensoriRosaPanchina.push(item);
                                        break;
                                    case 2:
                                        $scope.vm['squadra'+(index+1)].centrocampistiRosaPanchina.push(item);
                                        break;
                                    case 3:
                                        $scope.vm['squadra'+(index+1)].attaccantiRosaPanchina.push(item);
                                        break;
                                    case 4:
                                        $scope.vm['squadra'+(index+1)].allenatoriRosaPanchina.push(item);
                                        break;
                                };
                            });
                        } else {
                            $scope.vm['squadra'+(index+1)].hasFormazione = false;
                        }

                    });
                    $scope.vm.squadraSelected = $scope.vm.squadra1;
		        }
                $rootScope.hideLoading();
			},function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
			});
    	};

        $scope.setSquadraSelected = function(number){
            $scope.vm.squadraSelected = $scope.vm['squadra'+number];
        };

        $scope.goToDettaglioTabellino = function(giocatore){
            $rootScope.giocatore = giocatore;
            if (giocatore.cd_ruolo == 0)
                giocatore.icona = 'icon--portiere';
            else if (giocatore.cd_ruolo == 1)
                giocatore.icona = 'icon--difensore';
            else if (giocatore.cd_ruolo == 2)
                giocatore.icona = 'icon--centrocampista';
            else if (giocatore.cd_ruolo == 3)
                giocatore.icona = 'icon--attaccante';
            else if (giocatore.cd_ruolo == 4)
                giocatore.icona = 'icon--allenatore';

            if (window.cordova) $cordovaGoogleAnalytics.trackView('Tabellino');

            $state.go('app.dettaglioTabellino');
        };

        $scope.firstLetter = function(text: string){
            if(text)
                return text.charAt(0)+'.';
            else
                return '';
        };

        $ionicPopover.fromTemplateUrl('popoverPanchina.html', {
            scope: $scope,
        }).then(function(popover) {
            $scope.popoverPanchina = popover;
        });

    });
})();
