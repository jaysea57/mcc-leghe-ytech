cordova plugin add cordova-plugin-inappbrowser --save
cordova plugin add cordova-plugin-dialogs --save
cordova plugin add https://github.com/pwlin/cordova-plugin-file-opener2.git --save
cordova plugin add https://github.com/Paldom/SpinnerDialog.git --save
cordova plugin add cordova-plugin-file-transfer --save
cordova plugin add cordova-plugin-file --save
cordova plugin add cordova-plugin-google-analytics --save
cordova plugin add cordova-plugin-facebook4 --variable APP_ID="343417689117586" --variable APP_NAME="MisterCalcioCup - leghe di amici" --save
cordova plugin add cordova-plugin-firebase@0.1.22 --save
